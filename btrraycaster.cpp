#include<iostream>
#include<cmath>

//Includes for mygetch()
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

//Includes for SDL2
#include "SDL.h"

using namespace std;

const int FOV = 240;
const int width = 720;
const int height = 480;

Uint32 green;
Uint32 sky;
Uint32 red;
Uint32 brown;
Uint32 grayer;

int mygetch( ) {
  struct termios oldt,
                 newt;
  int            ch;
  tcgetattr( STDIN_FILENO, &oldt );
  newt = oldt;
  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newt );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
  return ch;
}

int ile(double length){
	return height/length;
	double a = (- height)/14;
	double b = height - a;
	return a*length + b;
}

struct dot{
	double x,y;
	double angle;
};
struct ray{
	double length;
	int end;
};
int map[10][10] =
	{
		{1,1,1,1,1,1,1,1,1,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,0,0,0,1,1,1,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,1,1,1,1,1,1,1,1,1}
	};

char screen[20][FOV];

//Rays to be cast
ray rays[FOV];

int move(dot &player, double speed){
	double newX = player.x + speed * sin(player.angle * M_PI / 180);
	double newY = player.y + speed * -cos(player.angle * M_PI / 180);
	player.x = newX;
	player.y = newY;
	int x = floor(newX);
	int y = floor(newY);
	if(newX>=0 and newX < 10 and newY >= 0 and newY < 10 and map[y][x] == 0){
		return 0;
	}
	return 1;
}
void rotate(dot &player, int angle){
	player.angle += angle;
	while(player.angle < 0)	player.angle += 360;
	while(player.angle > 360) player.angle -= 360;
	//player.angle %= 360;
}

void clearScreen(){
	cout<<"\033[H\033[J";
}
void clearWindow(SDL_Surface *s){
	SDL_FillRect(s, NULL, green);
	SDL_Rect rect;
	rect.x = rect.y = 0;
	rect.w = width;
	rect.h = height/2;
	SDL_FillRect(s, &rect, sky);
}

void castRays(dot &player){
	double angle = player.angle - FOV/8;
	for(int i=0;i<FOV;i++){
		ray tmpRay;
		dot rayPos;
		rayPos.x = player.x;
		rayPos.y = player.y;
		rayPos.angle = angle + i/4;
		int x = floor(rayPos.x);
		int y = floor(rayPos.y);
		while(map[y][x] == 0){
			move(rayPos,0.1);
			x = floor(rayPos.x);
			y = floor(rayPos.y);
		}
		tmpRay.end = map[y][x];
		//Accuracy backwards
		while(map[y][x] != 0){
			move(rayPos,-0.001);
			x = floor(rayPos.x);
			y = floor(rayPos.y);
		}
		//Od
		if(rayPos.y - y < 0.001) tmpRay.end = 2;
		if(rayPos.x - x < 0.001) tmpRay.end = 3;
		//Do
		tmpRay.length = sqrt( pow((player.x - rayPos.x), 2) + pow((player.y - rayPos.y), 2) );
		tmpRay.length *= cos((i/4 - FOV/8)*M_PI/180);
		rays[i] = tmpRay;
	}
}

void draw(dot &player, SDL_Surface *s, SDL_Window *window){
	castRays(player);
	//clearScreen();
	clearWindow(s);
	cout<<"x: "<<player.x<<", y: "<<player.y<<", angle: "<<player.angle<<endl<<endl;
	/*
	for(int i=0;i<20;i++)
		for(int j=0;j<FOV;j++)
			screen[i][j] = ' ';

	for(int i = 0; i < FOV; i++){
		int pasek = ile(rays[i].length);
		if(pasek>20) pasek=20;
		pasek /= 2;
		pasek *= 2;
		int start = (20-pasek)/2;
		for(int j = 0; j < pasek; j++){
			screen[start+j][i] = '#';
		}
	}*/
	for(int i = 0; i < FOV; i++){
		int pasek = ile(rays[i].length);
		if(pasek > height) pasek = height;
		if(pasek < 0) pasek = 0;
		int start = (height - pasek) / 2;
		int szer = (width) / FOV;
		SDL_Rect rect;
		rect.x = szer*i;
		rect.y = start;
		rect.w = szer;
		rect.h = pasek;
		Uint32 color;
		if(rays[i].end == 1) color = red;
		else if(rays[i].end == 2) color = brown;
		else if(rays[i].end == 3) color = grayer;
		SDL_FillRect(s, &rect, color);
	}
	SDL_UpdateWindowSurface(window);
	//for(int i=0;i<FOV;i++)
	//	cout<<ile(rays[i].length)<<endl;
	/*
	for(int i=0;i<20;i++){
		for(int j=0;j<FOV;j++)
			cout<<screen[i][j];
		cout<<endl;
	}*/
}
int main(){

	//Initialize player
	dot player;
	player.x = 5.5;
	player.y = 4.5;
	player.angle = 0;

	//initialize SDL2
	SDL_Window *window = NULL;
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("Raycaster",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,width,height,SDL_WINDOW_OPENGL);
	SDL_Event event;
	SDL_Surface *surf = NULL;
	surf = SDL_GetWindowSurface( window );
	green = SDL_MapRGB(surf->format, 0,150,0);
	sky = SDL_MapRGB(surf->format, 100,200,255);
	red = SDL_MapRGB(surf->format, 255,155,155);
	grayer = SDL_MapRGB(surf->format, 255,200,200);
	brown = SDL_MapRGB(surf->format, 200,100,100);
	bool running = true;

	bool w,s,a,d;
	w=s=a=d=false;
	while(running){
		while(SDL_PollEvent(&event)){
			if(event.type == SDL_QUIT){
				running = false;
				break;
			}
			if(event.type == SDL_KEYDOWN){
				if(event.key.keysym.scancode == SDL_SCANCODE_W)
					w=true;
				if(event.key.keysym.scancode == SDL_SCANCODE_S)
					s = true;
				if(event.key.keysym.scancode == SDL_SCANCODE_A)
					a = true;
				if(event.key.keysym.scancode == SDL_SCANCODE_D)
					d = true;
				if(event.key.keysym.scancode == SDL_SCANCODE_X)
					running = false;
			}
			if(event.type == SDL_KEYUP){
				if(event.key.keysym.scancode == SDL_SCANCODE_W)
					w=false;
				if(event.key.keysym.scancode == SDL_SCANCODE_S)
					s = false;
				if(event.key.keysym.scancode == SDL_SCANCODE_A)
					a = false;
				if(event.key.keysym.scancode == SDL_SCANCODE_D)
					d = false;
			}
		}
		//a = mygetch();
		int movement;
		if(w){
			movement = move(player,0.1);
			if(movement == 1) move(player, -0.1); //Wall
		}
		if(s){
			movement = move(player, -0.1);
			if(movement == 1) move(player, 0.1); //Wall
		}
		if(a) rotate(player, -5);
		if(d) rotate(player,5);
		draw(player,surf, window);
		SDL_Delay(20);
		//cout<<a;
	}
	
	return 0;
}