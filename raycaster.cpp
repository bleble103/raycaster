#include<iostream>
#include<cmath>

//Includes for mygetch()
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

using namespace std;

const int FOV = 60;

int mygetch( ) {
  struct termios oldt,
                 newt;
  int            ch;
  tcgetattr( STDIN_FILENO, &oldt );
  newt = oldt;
  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newt );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
  return ch;
}

int ile(double length){
	return 20/length;
	return (-1.6)*length + 18;
}

struct dot{
	double x,y;
	int angle;
};
struct ray{
	double length;
	int end;
};
int map[10][10] =
	{
		{1,1,1,1,1,1,1,1,1,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,1,0,0,0,0,0,0,1},
		{1,0,0,0,0,1,1,1,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,1,1,1,1,1,1,1,1,1}
	};

char screen[20][FOV];

//Rays to be cast
ray rays[FOV];

int move(dot &player, double speed){
	double newX = player.x + speed * sin(player.angle * M_PI / 180);
	double newY = player.y + speed * -cos(player.angle * M_PI / 180);
	player.x = newX;
	player.y = newY;
	int x = floor(newX);
	int y = floor(newY);
	if(newX>=0 and newX < 10 and newY >= 0 and newY < 10 and map[y][x] == 0){
		return 0;
	}
	return 1;
}
void rotate(dot &player, int angle){
	player.angle += angle;
	while(player.angle < 0)	player.angle += 360;
	player.angle %= 360;
}

void clearScreen(){
	cout<<"\033[H\033[J";
}

void castRays(dot &player){
	int angle = player.angle - FOV/2;
	for(int i=0;i<FOV;i++){
		ray tmpRay;
		dot rayPos;
		rayPos.x = player.x;
		rayPos.y = player.y;
		rayPos.angle = angle + i;
		int x = floor(rayPos.x);
		int y = floor(rayPos.y);
		while(map[y][x] == 0){
			move(rayPos,0.1);
			x = floor(rayPos.x);
			y = floor(rayPos.y);
		}
		tmpRay.end = map[y][x];
		tmpRay.length = sqrt( pow((player.x - rayPos.x), 2) + pow((player.y - rayPos.y), 2) );
		tmpRay.length *= cos((i - FOV/2)*M_PI/180);
		rays[i] = tmpRay;
	}
}

void draw(dot &player){
	castRays(player);
	clearScreen();
	cout<<"x: "<<player.x<<", y: "<<player.y<<", angle: "<<player.angle<<endl<<endl;
	for(int i=0;i<20;i++)
		for(int j=0;j<FOV;j++)
			screen[i][j] = ' ';

	for(int i = 0; i < FOV; i++){
		char wall = '#';
		if(rays[i].length > 1) wall = '&';
		if(rays[i].length > 3) wall = '+';
		int pasek = ile(rays[i].length);
		if(pasek>20) pasek=20;
		pasek /= 2;
		pasek *= 2;
		int start = (20-pasek)/2;
		for(int j = 0; j < pasek; j++){
			screen[start+j][i] = wall;
		}
	}
	
	//for(int i=0;i<FOV;i++)
	//	cout<<ile(rays[i].length)<<endl;
	
	for(int i=0;i<20;i++){
		for(int j=0;j<FOV;j++)
			cout<<screen[i][j];
		cout<<endl;
	}
}
int main(){

	//Initialize player
	dot player;
	player.x = 5.5;
	player.y = 4.5;
	player.angle = 0;

	char a = '0';
	while(a != 'x'){
		a = mygetch();
		int movement;
		switch(a){
			case 'w':
				movement = move(player,0.1);
				if(movement == 1) move(player, -0.1); //Wall
				break;
			case 's':
				movement = move(player, -0.1);
				if(movement == 1) move(player, 0.1); //Wall
				break;
			case 'a':
				rotate(player, -5);
				break;
			case 'd':
				rotate(player,5);
				break;
		};
		draw(player);
		//cout<<a;
	}
	
	return 0;
}